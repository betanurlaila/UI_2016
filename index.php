<?php include('header.php') ?>
<div class="welcome animated rubberBand">
  <center><h2>Selamat Datang di PPDB ONLINE TULUNGAGUNG 2016</h2>
    <h4>Situs untuk melakukan pendaftaran SMP/SMA/SMK secara ONLINE</h4></center>
</div>
<div class="info">
	<div class="col-sm-offset-2 col-sm-8 col-sm-offset-2">
		<div class="carousel slide text-center" id="info-carousel">
      <ol class="carousel-indicators">
        <li data-target="#info-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#info-carousel" data-slide-to="1"></li>
        <li data-target="#info-carousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <br>
          <h4>PEMBERITAHUAN (PENTING)</h4>
          <p>Himbauan bagi para peserta PPDB,Pastikan bahwa No.Ujian dan password yang anda terima sudah benar.Apabila Anda mengalami kesalahan No.Ujian/password segera hubungi kami di call center yang tertera pada <a href="#">kontak</a>.Atau Anda bisa langsung datang ke <a href="#">kantor</a> kami dengan membawa SKHUN asli.</p>
        </div>
        <div class="item">
          <br>
          <h4>PEMBERITAHUAN (PENTING)</h4>
          <p>Atas pentingnya pemberitahuan tersebut,bagi yang melihat pemberitahuan ini harap menginformasikan kepada teman-teman yang lain (peserta PPDB lain)Kami tidak akan bertanggung jawab atas masalah yang timbul saat PPDB online telah ditutup.Informasi lebih lanjut hubungi <a href="">kontak</a>!</p>
        </div>
        <div class="item ">
          <br>
          <h4>TENTANG PPDB ONLINE</h4>
          <p>PPDB online merupakan sarana yang digunakan untuk membantu siswa melakukan pendaftaran ke sekolah yang diinginkan serta membantu pihak sekolah dalam meleakukan seleksi terhadap siswa-siswa yang melakukan pendaftaran.Pendafataran online akan dilaksanakan pada 20-24 Juni 2016 <a href="">(jadwal)</a>.</p>
        </div>
      </div>
    </div>
	</div>
</div>
<div class="col-sm-offset-3 col-sm-3 col-sm-offset-6">
  <div class="panel panel-default">
  <div class="panel-body">
    <center><a href=""><img src="assets/images/seragam_smp.png" width="80%" height="100%"></a></center>
  </div>
  <div class="panel-footer">
    <h4><a href="">Pendaftaran SMP</a></h4>
  </div>
</div>
</div>
<?php include('footer.php') ?>