<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PPDB ONLINE 2016</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/prettyPhoto.css">
    <link rel="stylesheet" href="assets/css/price-range.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/font-awesome-4.5.0/css/font-awesome.min.css">
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.scrollUp.min.js"></script>
    <script src="assets/js/price-range.js"></script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/app.min.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Informasi<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Alur Pendaftaran</a></li>
                <li><a href="#">Aturan</a></li>
                <li><a href="#">Jadwal</a></li>
                <li><a href="#">Pagu SMP</a></li>
                <li><a href="#">Pagu SMA</a></li>
                <li><a href="#">Pagu SMK</a></li>
                <li><a href="#">Daftar Siswa SD/Mi</a></li>
                <li><a href="#">Daftar Siswa SMP/Mts</a></li>
              </ul>
            </li>
            <li><a href="#">FAQ</a></li>
            <li><a href="kontak.php">Kontak</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="home-image">
      <img src="assets/images/depan.png" alt="">
    </div>
    <div class="server-time animated pulse">
      <div class="col-sm-offset-1 col-sm-3">
        <div class="panel panel-default">
          <div class="panel-body">
            <center><i class="fa fa-clock-o"></i></center>
            <center><h4>Waktu Server</h4>
            <p></p></center>
          </div>
        </div>
      </div>
    </div>
    <div class="open-time animated pulse">
      <div class="col-sm-offset-0 col-sm-3">
        <div class="panel panel-default">
          <div class="panel-body">
            <center><i class="fa fa-clock-o"></i></center>
            <center><h4>Pembukaan Pendaftaran</h4>
            <p></p></center>
          </div>
        </div>
      </div>
    </div>
    <div class="close-time animated pulse">
      <div class="col-sm-offset-0 col-sm-3 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-body">
            <center><i class="fa fa-clock-o"></i></center>
            <center><h4>Penutupan Pendaftaran</h4>
            <p></p></center>
          </div>
        </div>
      </div>
    </div>
    <br><br>
  </body>
</html>